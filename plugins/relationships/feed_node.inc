<?php

/**
 * @file
 * Plugin to provide an relationship handler for feed node.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Feed node'),
  'keyword' => 'feed_node',
  'description' => t('Adds a feed node from a node context.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'aggregatorplus_feed_node_context',
  'defaults' => array(),
);

/**
 * Return a new context based on an existing context.
 */
function aggregatorplus_feed_node_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node');
  }

  if (isset($context->data->feed_nid)) {
    // Just load the parent book.
    $nid = $context->data->feed_nid;

    if (!empty($nid)) {
      // Load the node.
      $node = node_load($nid);
      // Generate the context.
      return ctools_context_create('node', $node);
    }
  }
  else {
    return ctools_context_create_empty('node');
  }
}
